using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ScreenStateEnum
{
    None,
    Start,
    Menu,
    Loading,
    Prologue,
    Game,
    Dialogue,
    Pause,
    Store,
    Death,
    Prestige
}

public class ScreenState
{
    static public ScreenStateEnum state;
}