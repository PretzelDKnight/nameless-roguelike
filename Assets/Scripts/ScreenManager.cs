using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class ScreenManager : MonoBehaviour
{
    static public ScreenManager instance = null;
    [SerializeField] string[] gameScenes;

    bool busy;

    void Start()
    {
        if (instance != null)
            Destroy(gameObject);
        else
            instance = this;

        Init();
    }

    private void Init()
    {
        busy = false;
        ScreenState.state = ScreenStateEnum.Game;
    }

    private void Update()
    {
        //switch (ScreenState.state)
        //{
        //    case ScreenStateEnum.None:
        //        StartCoroutine(AsyncSceneLoad(gameScenes[0], ScreenStateEnum.Game));
        //        break;
        //    default:
        //        break;
        //}
    }

    IEnumerator AsyncSceneLoad(string str, ScreenStateEnum state)
    {
        if (!busy)
        {
            busy = true;
            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(str, LoadSceneMode.Additive);

            while (!asyncLoad.isDone)
            {
                yield return null;
            }

            busy = false;
            ScreenState.state = state;
        }
    }

    IEnumerator AsyncUnloadWhileLoad(string load, string unload, ScreenStateEnum state)
    {
        if (!busy)
        {
            busy = true;
            SceneManager.UnloadSceneAsync(unload);
            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(load, LoadSceneMode.Additive);

            while (!asyncLoad.isDone)
            {
                yield return null;
            }

            busy = false;
            ScreenState.state = state;
        }
    }
}