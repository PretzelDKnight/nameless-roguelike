using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class Buff
{
    protected float duration;

    abstract public void OnHit();

    abstract public void Function();

    abstract public void Update();
}