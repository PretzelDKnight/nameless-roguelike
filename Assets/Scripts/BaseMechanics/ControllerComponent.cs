using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public enum PlayerState
{
    Idle,
    Moving,
    Attacking,
    Dash,
    AnimationBlock
}

public abstract class ControllerComponent : MonoBehaviour
{
    static protected Animator animator;
    static new protected Rigidbody rigidbody;

    static protected PlayerState state;

    static protected Vector3 Right = new Vector3(-1, 0, 1);
    static protected Vector3 Forward = new Vector3(-1, 0, -1);

    static protected PlayerData data;

    static protected Vector2 moveVal;


    protected void Start()
    {
        data = Resources.Load<PlayerData>("PlayerData");
        rigidbody = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        state = PlayerState.Idle;

        Init();
    }

    abstract protected void Init();

    abstract public void OnCall(InputAction.CallbackContext context);
}