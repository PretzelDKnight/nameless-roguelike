using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using System;

public class MoveComponent : ControllerComponent
{
    [SerializeField] float acceleration;
    [SerializeField] float turnSpeed;

    protected override void Init()
    {

    }

    private void LateUpdate()
    {
        Vector3 displacement = Right * moveVal.x + Forward * moveVal.y;
        displacement = displacement.normalized * data.spd;
        float magnitude = rigidbody.velocity.magnitude / data.spd;

        switch (state)
        {
            case PlayerState.Moving:
                {
                    rigidbody.velocity += displacement * Time.deltaTime * acceleration;

                    if (Mathf.Abs(moveVal.x) + Mathf.Abs(moveVal.y) != 0)
                        transform.forward = Vector3.RotateTowards(transform.forward, displacement.normalized, turnSpeed * Time.deltaTime, 360);

                    if (magnitude > 1.75f)
                        rigidbody.velocity -= rigidbody.velocity * Time.deltaTime * acceleration * 0.5f;
                    else if (magnitude >= 1f)
                        rigidbody.velocity = displacement;

                    break;
                }
            case PlayerState.Idle:
                {
                    if (moveVal.sqrMagnitude > 0)
                        state = PlayerState.Moving;
                    else
                        rigidbody.velocity -= rigidbody.velocity * Time.deltaTime * acceleration;
                    break;
                }
            case PlayerState.Attacking:
                {
                    rigidbody.velocity -= rigidbody.velocity * Time.deltaTime * acceleration;
                    break;
                }
            case PlayerState.Dash:
                {
                    if (Mathf.Abs(moveVal.x) + Mathf.Abs(moveVal.y) != 0)
                        transform.forward = Vector3.RotateTowards(transform.forward, displacement.normalized, 6 * Time.deltaTime, 30);
                    break;
                }
            default:
                break;
        }

        animator.SetFloat("Move", magnitude);
        rigidbody.angularVelocity = Vector3.zero;
    }

    public override void OnCall(InputAction.CallbackContext context)
    {
        moveVal = context.ReadValue<Vector2>();

        if (ScreenState.state == ScreenStateEnum.Game)
            if (state != PlayerState.Attacking && state != PlayerState.AnimationBlock && state != PlayerState.Dash)
                if (moveVal.sqrMagnitude > 0)
                    state = PlayerState.Moving;
                else
                    state = PlayerState.Idle;
    }
}