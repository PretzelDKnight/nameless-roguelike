using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/PlayerData", order = 1)]
public class PlayerData : ScriptableObject
{
    public int hp;
    public float str;
    public float spd;
    public float atkspd;
    public int karma;

    public int dashCharges;
    public float dashSpeed;
}
