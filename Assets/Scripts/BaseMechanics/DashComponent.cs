using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.InputSystem;

public class DashComponent : ControllerComponent
{
    [Header("Dash Values")]
    [SerializeField] float dashDuration;
    [SerializeField] float dashAcceleration;
    [SerializeField] float dashComboInterval;
    [SerializeField] float dashGap;
    [NonSerialized] public int dashCount;
    float dashTimer;
    float dashComboTimer;

    protected override void Init()
    {
        dashCount = data.dashCharges;
    }

    void Update()
    {
        if (dashTimer <= dashGap)
            dashTimer += Time.deltaTime;

        if (dashComboTimer <= dashComboInterval)
        {
            dashComboTimer += Time.deltaTime;
            if (dashComboTimer > dashComboInterval)
                dashCount = data.dashCharges;
        }
    }

    IEnumerator DashCoroutine()
    {
        state = PlayerState.Dash;

        float time = 0;
        dashCount -= 1;
        dashTimer = 0;
        dashComboTimer = 0;

        animator.SetBool("Dash", true);

        while (time <= 1.1f)
        {
            time += Time.deltaTime / dashDuration;
            rigidbody.velocity += transform.forward * data.dashSpeed * Time.deltaTime * dashAcceleration;
            if (rigidbody.velocity.magnitude > data.dashSpeed)
                rigidbody.velocity = transform.forward * data.dashSpeed;
            yield return null;
        }

        state = PlayerState.Idle;
        animator.SetBool("Dash", false);
        rigidbody.velocity = Vector3.zero;
    }

    public override void OnCall(InputAction.CallbackContext context)
    {
        if (ScreenState.state == ScreenStateEnum.Game)
        {
            if (context.ReadValueAsButton())
                if (dashCount > 0)
                    if (state != PlayerState.AnimationBlock && state != PlayerState.Dash)
                        if (dashTimer > dashGap)
                            StartCoroutine(DashCoroutine());
        }
    }
}