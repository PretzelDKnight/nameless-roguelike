using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CamState
{
    NonTargetted,
    Targetted
}

public class CameraScript : MonoBehaviour
{
    [SerializeField] Transform target;
    [SerializeField] float smoothing = 5f;
    [SerializeField] Vector3 offset;

    public CamState state;

    void Start()
    {
        NonTargetCam();
    }

    void LateUpdate()
    {
        switch(state)
        {
            case CamState.Targetted: TargettedMove();
                break;
            default: 
                if (ScreenState.state == ScreenStateEnum.Game)
                {
                    target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
                    TargetCam(target);
                }
                break;
        }
    }

    public void TargetCam(Transform target)
    {
        transform.position = target.position + offset;
        transform.LookAt(target);
        state = CamState.Targetted;
    }

    public void NonTargetCam()
    {
        transform.position = Vector3.zero;
        state = CamState.NonTargetted;
    }

    void TargettedMove()
    {
        Vector3 targetCamPos = target.position + offset;
        transform.position = Vector3.Lerp(transform.position, targetCamPos, smoothing * Time.deltaTime);
    }
}