using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PrevAttack
{
    None,
    Attack,
    Special
}

abstract public class Weapon
{
    protected Animator anim;
    protected WeaponData data;
    protected PrevAttack currentAttack;

    abstract public void Init();
    abstract public void Update();
    abstract public void AttackAnimation();
    abstract public void SpecialAnimation();
}
