using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandardWeapon : Weapon
{
    int currentClip;
    float comboTime;
    float attackTime;

    public StandardWeapon(MoveComponent player)
    {
        data = Resources.Load<WeaponData>("StandardWeaponData");
        //anim = player.playerAnim.anim;
        currentClip = 0;

        //Init();
    }

    public override void Init()
    {
    }

    public override void AttackAnimation()
    {
        if (attackTime > data.attackInterval)
        {
            if (comboTime < data.comboInterval)
            {
                if (currentAttack != PrevAttack.Special)
                    currentClip = 0;

                switch (currentClip)
                {
                    case 0:
                        //anim.Play(data.attackClip[0]);
                        currentClip++;
                        break;
                    case 1:
                        //anim.Play(data.attackClip[1]);
                        currentClip++;
                        break;
                    case 2:
                        //anim.Play(data.attackClip[2]);
                        currentClip = 0;
                        break;
                    default:
                        break;
                }
            }

            Debug.Log("Attacking");
            comboTime = 0;
            attackTime = 0;
            currentAttack = PrevAttack.Attack;
        }
    }

    public override void SpecialAnimation()
    {
        Debug.Log("Standard Special");
    }

    public override void Update()
    {
        comboTime += Time.deltaTime;
        attackTime += Time.deltaTime;
    }
}
