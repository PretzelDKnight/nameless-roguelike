using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "WeaponData", menuName = "ScriptableObjects/WeaponData", order = 2)]
public class WeaponData : ScriptableObject
{
    public GameObject prefab;

    public float comboInterval;

    public float attackInterval;

    public AnimationClip[] attackClip;

    public AnimationClip specialClip;
}
