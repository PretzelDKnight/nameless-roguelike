using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class SkillNode
{
    protected string ID; 

    protected SkillNode[] children;

    protected SkillNode parent;

    abstract public void Passive();

    abstract public void Active();

    abstract public void Update();
}
