using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Save
{
    public string saveCheck;
    public int run;
    public int pcurrency;
    public string[] currentbuffs;
    public KeyValuePair<string, int>[] weapons;
    public string skilltree;
    public string[] skills;
    public string[] quests;

    public int mobkills;
    public int deaths;
    public int pcurrencySpent;
}
