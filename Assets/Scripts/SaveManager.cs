using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class SaveManager : MonoBehaviour
{
    static public SaveManager instance = null;

    static string saveCheck = "pokemon";
    static bool saveLoaded = false;

    void Start()
    {
        if (instance != null)
            Destroy(gameObject);
        else
            instance = this;

        Init();
    }

    // (TD) ::Neccessity of this method
    private void Init()
    {
        LoadGame();
    }

    void SaveGame()
    {
        Save sav = CreateSaveFile();

        string json = JsonUtility.ToJson(sav);
        BinaryFormatter bf = new BinaryFormatter();

        FileStream file = File.Create(Application.persistentDataPath + "/gamesave.save");
        bf.Serialize(file, json);
        file.Close();
    }

    void LoadGame()
    {
        if (File.Exists(Application.persistentDataPath + "/gamesave.save"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/gamesave.save", FileMode.Open);
            string json = (string)bf.Deserialize(file);
            Save sav = JsonUtility.FromJson<Save>(json);
            file.Close();

            if (sav.saveCheck != saveCheck)
                return;

            LoadSaveFile(sav);
            saveLoaded = true;
        }
    }

    // (TD) ::Create save file from data
    Save CreateSaveFile()
    {
        Save sav = new Save();

        sav.saveCheck = saveCheck;

        return sav;
    }

    // (TD) ::Load data from save file
    void LoadSaveFile(Save sav)
    {

    }

    public bool SavePresent
    { get { return saveLoaded; } }
}